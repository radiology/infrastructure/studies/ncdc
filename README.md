### MRI defacing pipeline

This defacing tool is an automated pipeline that defaces all defacable MRI data in a project stored in XNAT. 
This pipeline uses Elastix registration tool and Free surfer's defacing tool, reviews and effects of which, 
on clinically relevant measurements is discussed in the article by [A. de Sitter et al](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6957560/).

### Documentation
Documentation for this defacing pipleline can be found in [readthedocs](https://mri-defacing-pipeline.readthedocs.io/en/stable/).

## Getting Started

This pipeline runs for linux based distributions. So far, it does not have a support for windows machines.

### Prerequisites

Download and install docker 

```
sudo apt-get update
```
```
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### Installing

Clone this repository and build docker image
```
git clone https://gitlab.com/radiology/studies/ncdc.git
```
Navigate to the location of the cloned repository

```
cd <repository>/ncdc  
```
Build the docker image
```
docker-compose build
```
Run the pileline
```
docker-compose run fs_deface <your-XNAT-URL> <Project name> <T1 reference name>
```

example:
```
docker-compose run fs_deface https://bigr-rad-xnat.erasmusmc.nl DefacingData T1
```
It will ask you your credentials,
    ie- username
        password
if log in is sucessful, you will see defacing pipeline starting on your project data.


## Versioning

We use a simple Major-Minor versioning. For the versions available, see the [tags on this repository](https://gitlab.com/radiology/infrastructure/studies/ncdc/-/tags). 

## Authors

* **Mahlet Birhanu** - *Initial work* - 

See also the list of [contributors](https://gitlab.com/radiology/infrastructure/studies/ncdc/-/graphs/master) who participated in this project.

## License

This project is licensed under the Apache License 2.0 - see the [APACHE-LICENSE-2.0](APACHE-LICENSE-2.0) file for details

## Acknowledgments

* BIGR infrustructure team
