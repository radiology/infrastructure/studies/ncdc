.. deface documentation master file, created by
   sphinx-quickstart on Mon Feb 17 16:41:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Defacing tool documentation!
==================================

.. toctree::
   :maxdepth: 2

   introduction.rst
   fastr_tools.rst
   howtorun.rst
   references.rst
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
