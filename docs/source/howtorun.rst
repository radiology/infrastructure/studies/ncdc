****************
How to run tool
****************

**Download and install docker (if its not installed already)**

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

Option 1:  Clone this repository and build docker image
========================================================

#   git clone https://gitlab.com/radiology/studies/ncdc.git
#   cd home/ncdc  #(or to the location of the cloned repository)
#   docker-compose build
#   docker-compose run fs_deface <your-XNAT-URL> <Project name>

example:
    docker-compose run fs_deface https://bigr-rad-xnat.erasmusmc.nl DefacingData

It will ask you your credentials,
    ie- xnat-URL
        username:
        password:
        
if log in is sucessful, you will see defacing pipeline starting on your project data.


Option 2: download docker image from docker hub
======================================================

#TODO
