************
Fastr_tools
************

The pipelines uses fastr to create wrappers around executables and python scripts.

dcm2nii
==================
Wrapps MRICron's dicom to nifti converter for preprocessing.


deface
==================
wraps Free Surfer's deface tool to deface T1 (or T1 registered) samples pulled from xnat.


elastix
==================
wraps elastix tool for registration of non-T1 samples to T1.


ExtractMask
==================
wraps a python script that extracts masks from defaced samples.


