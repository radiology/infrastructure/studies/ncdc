************
Introduction
************

Brain data sharing can be risky as it has been found out that MRI gives a good enough skin
to air contrast and spatial resolution to perform facial recognition from a 3D rendered version
of the image (A. de Sitter et al). 

This defacing tool is an automated pipeline that defaces all defacable MRI data in a project 
stored in XNAT. While developing this tool, Free surfer's defacing tool is used in this pipeline,
reviews and effects of which, on clinically relevant measurements is discussed in the article by A. de Sitter et al.

Defacing Test data: 
The pipeline was run and tested on a tesy Project was created on Erasmus MC radiology xnat containing
representative subjects from CVON Hart-Brein, Parelsnoer-NDZ, BBMRI ageing brain and ADNIatBIGR.

    Input: MRI Brain scans of subjects from an XNAT project
    Output: Defaced scans uploaded to the same project 

Filters Used:
The pipeline defaces all relevant MRI brain scans with the exclusion of survey scans (or any scan with
less than a threshold (10) of number of slices).
The pipleine currently does not support defacing 4D MRI data (ASL SENSE, SWE_PI 2 min, etc...)

Overview
==================
The basic overview of this defacing pipeline is shown in the flow chart below.

.. image:: static/general_overview.png
