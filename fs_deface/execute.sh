#!/bin/bash

XNATHOST=$1
PROJECT=$2
T1_NAME=$3
LOG_INFO=True

#sign into project
./sign_in.sh $XNATHOST

#run deface script
python3.6 deface.py --host $XNATHOST --project $PROJECT --t1_name $T1_NAME --log_INFO $LOG_INFO 
