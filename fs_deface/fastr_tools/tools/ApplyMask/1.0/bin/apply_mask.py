import numpy as np 
import nibabel as nib

def apply_mask (input_path, mask_path, result_path = None):
    ''' This function applies mask from extracted from a defaced image '''

    try:
        input = nib.load(input_path)
        input_image = input.get_fdata()
    except IOError:
        print ('Could not read file: {}'.format(input_path))

    try:
        mask_image = (nib.load(mask_path)).get_fdata()
    except IOError:
        print ('Could not read file: {}'.format(mask_path))
        
    if len(input_image.shape) > 3:
        input_image = input_image[:,:,:,0]

    if (input_image).shape != mask_image.shape:
        print ('Input image and mask shapes do not match: {} does not match {}'.format(input_image.shape, mask_image.shape))
    try:
        defaced = np.multiply(input_image , (1- mask_image))
    except ValueError:
        print ('Unable to perform operation:  shapes {} does not match {}'.format(input_image.shape, mask_image.shape))
        return
    defaced_file = nib.Nifti1Image(defaced, input.affine)

    if result_path is None:
        result_path = './defaced_result.nii.gz'
    
    try:
        nib.save(defaced_file, result_path)
    except IOError:
        print ('Could not write to file: {}'.format(result_path))

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Apply mask ')
    parser.add_argument('-i', '--input_path', type=str, help="Path pointing to the input image", required=True)
    parser.add_argument('-m', '--mask_path', type=str, help="Path pointing to mask", required=True)
    parser.add_argument('-o', '--result_path', type=str, help=" path/name of the result mask", required=False)
    args = parser.parse_args()

    if args.input_path:
        print ('Using\nInput image path: {}'.format(args.input_path))
    if args.mask_path:
        print ('Mask path: {}'.format(args.mask_path))

    apply_mask (args.input_path, args.mask_path, args.result_path)

if __name__ == "__main__":
    main()