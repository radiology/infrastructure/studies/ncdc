import numpy as np 
import nibabel as nib

def extract_mask (input_path, defaced_path, result_path = None):
    ''' This function extracts mask from a full (non-defaced) image and a defaced image using a simple numpy subtraction'''

    try:
        input = nib.load(input_path)
        input_image = input.get_fdata()
    except IOError:
        print ('Could not read file: {}'.format(input_path))

    try:
        defaced_image = (nib.load(defaced_path)).get_fdata()
    except IOError:
        print ('Could not read file: {}'.format(defaced_path))
    
    if len(input_image.shape) > 3:     
        input_image = input_image[:,:,:,0]
    
    if (input_image).shape != defaced_image.shape:
        print ('Input and Target shapes do not match: {} does not match {}'.format(input_image [:,:,:,0]).shape, defaced_image.shape)
        return
    
    mask = np.subtract(input_image ,defaced_image)
    binary_mask = np.subtract(1, np.array((mask < 100).astype(float)))
    mask_file = nib.Nifti1Image(binary_mask, input.affine)

    if result_path is None:
        result_path = './result-mask.nii.gz'
    
    try:
        nib.save(mask_file, result_path)
    except IOError:
        print ('Could not write to file: {}'.format(result_path))

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Extract mask ')
    parser.add_argument('-i', '--input_path', type=str, help="Path pointing to the input image", required=True)
    parser.add_argument('-d', '--defaced_path', type=str, help="Path pointing to the target (defaced) image", required=True)
    parser.add_argument('-o', '--result_path', type=str, help=" path/name of the result mask", required=False)
    args = parser.parse_args()

    if args.input_path:
        print ('Using\nInput image path: {}'.format(args.input_path))
    if args.defaced_path:
        print ('Defaced image path: {}'.format(args.defaced_path))

    extract_mask (args.input_path, args.defaced_path, args.result_path)

if __name__ == "__main__":
    main()