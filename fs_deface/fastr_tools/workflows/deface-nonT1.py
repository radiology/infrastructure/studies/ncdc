# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import fastr

fastr.config.log_to_file = True
dcmniix_supported_syntax = ['1.2.840.10008.1.2.4.90']

def create_network(isDICOM = True, nont1_syntax=None, T1_dcm = None, t1_syntax=None):
    network = fastr.create_network(id="deface")

    # load template images for defacing 
    brain_template = network.create_source('GCAImageFile', id='brain_template')
    face_template = network.create_source('GCAImageFile', id='face_template')
    
    # Load non-t1 scan (Source/moving image)
    if isDICOM:
        source_image = network.create_source('DicomImageFile', id='source_image')

        # Dicom to nifti conversions for non-t1 scan
        if nont1_syntax in dcmniix_supported_syntax:
            source_dcm2nii = network.create_node('dcm2niix/DicomToNifti:0.2', id='source_dcm2nii', tool_version ='0.2')
        else:
            source_dcm2nii = network.create_node('dcm2nii/DicomToNiftiLongPath:0.2', id='source_dcm2nii', tool_version ='0.2')

        source_dcm2nii.inputs['dicom_image'] = source_image.output
    else:
        source_image = network.create_source('NiftiImageFile', id='source_image')


    # Load T1 scan (Fixed image)
    if not T1_dcm:
        fixed_image = network.create_source('NiftiImageFile', id='fixed_image')
        
    else:
        fixed_image = network.create_source('DicomImageFile', id='fixed_image')
        # Dicom to nifti conversions for source t1 scan
        if t1_syntax in dcmniix_supported_syntax:
            fixed_dcm2nii = network.create_node('dcm2niix/DicomToNifti:0.2', id='fixed_dcm2nii', tool_version ='0.2')
        else:
            fixed_dcm2nii = network.create_node('dcm2nii/DicomToNiftiLongPath:0.2', id='fixed_dcm2nii', tool_version ='0.2')
        fixed_dcm2nii.inputs['dicom_image'] = fixed_image.output

 
    deface = network.create_node('deface/deface:1.0', id='deface', tool_version ='1.0')
    params = network.create_source('ElastixParameterFile', id='param_file')
    elastix_node = network.create_node('elastix/Elastix:4.8', id='elastix',  tool_version ='0.2')

    # Register non-t1 scan to T1

    # moving image is non-T1 (source image)
    if  not isDICOM:
        elastix_node.inputs['moving_image'] = source_image.output
    else:
        elastix_node.inputs['moving_image'] = source_dcm2nii.outputs['image']

    # fixed image is T1
    if  not T1_dcm:
        elastix_node.inputs['fixed_image'] = fixed_image.output
    else:
        elastix_node.inputs['fixed_image'] = fixed_dcm2nii.outputs['image']

    elastix_node.inputs['parameters'] = params.output

    # apply T1 mask here to deface
    #deface = network.create_node('image_analysis/tools/ApplyMask/ApplyMask:1.0', id='deface', tool_version ='1.0')
    #deface.inputs['input'] = elastix_node.outputs['image']
    #deface.inputs['mask'] = T1_mask.output
    
    # Deface registered scan
    deface.inputs['compressed'] =  elastix_node.outputs['image']
    deface.inputs['brain_template'] = brain_template.output
    deface.inputs['face_template'] = face_template.output

    # Save result
    defacedMRI = network.create_sink('NiftiImageFileCompressed', id='defacedMRI')
    defacedMRI.inputs['input'] = deface.outputs['image']

 
    return network


 
    