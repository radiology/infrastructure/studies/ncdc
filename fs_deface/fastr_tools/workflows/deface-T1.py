# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import fastr

fastr.config.log_to_file = True
dcmniix_supported_syntax = ['1.2.840.10008.1.2.4.90']

def create_network(isDICOM = True, t1_syntax=None, T1_dcm = None, nont1_syntax=None):
    network = fastr.create_network(id="deface")
    # Source images
    brain_template = network.create_source('GCAImageFile', id='brain_template')
    face_template = network.create_source('GCAImageFile', id='face_template')

    if isDICOM:
        source_image = network.create_source('DicomImageFile', id='source_image')

        # Dicom to nifti conversions
        if t1_syntax in dcmniix_supported_syntax:
            source_dcm2nii = network.create_node('dcm2niix/DicomToNifti:0.2', id='source_dcm2nii', tool_version ='0.2')
        else:
            source_dcm2nii = network.create_node('dcm2nii/DicomToNiftiLongPath:0.2', id='source_dcm2nii', tool_version ='0.2')

        source_dcm2nii.inputs['dicom_image'] = source_image.output
    else:
        source_image = network.create_source('NiftiImageFile', id='source_image')

    # load template images for defacing 
    
    deface = network.create_node('deface/deface:1.0', id='deface', tool_version ='1.0')

    # deface image
    if isDICOM and t1_syntax not in dcmniix_supported_syntax:
        deface.inputs['compressed'] = source_dcm2nii.outputs['image']
    elif isDICOM and t1_syntax in dcmniix_supported_syntax:
        deface.inputs['uncompressed'] = source_dcm2nii.outputs['image']
    else:
        deface.inputs['uncompressed'] = source_image.output

    deface.inputs['brain_template'] = brain_template.output
    deface.inputs['face_template'] = face_template.output

    #extract mask
    extract_mask = network.create_node('ExtractMask/ExtractMask:1.0', id='extract_mask', tool_version ='1.0')
    if isDICOM:
        extract_mask.inputs['input'] = source_dcm2nii.outputs['image']
    else:
        extract_mask.inputs['input_nii'] = source_image.output

    extract_mask.inputs['defaced'] = deface.outputs['image']

    # Save result
    defacedMRI = network.create_sink('NiftiImageFileCompressed', id='defacedMRI')
    defacedMRI.inputs['input'] = deface.outputs['image']

    defacedMask = network.create_sink('NiftiImageFileCompressed', id='defacedMask')
    defacedMask.inputs['input'] = extract_mask.outputs['image']
    return network


 
    