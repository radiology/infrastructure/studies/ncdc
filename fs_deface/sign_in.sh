#!/bin/bash

proto="$(echo $1 | grep :// | sed -e's,^\(.*://\).*,\1,g')"
# remove the protocol
url="$(echo ${1/$proto/})"
echo "***** SIGN IN *****"
echo "XNAT: $url"
echo username:
read login

echo password:
password=''
while IFS= read -r -s -n1 char; do
  [[ -z $char ]] && { printf '\n'; break; }
  if [[ $char == $'\x7f' ]]; then 
      [[ -n $password ]] && password=${password%?}
      printf '\b \b' 
  else
    password+=$char
    printf '*'
  fi
done

FILE=~/.netrc

/bin/cat >> $FILE <<EOL
machine $url
login ${login}
password ${password}
EOL

chown root:root $FILE
chmod 600 $FILE