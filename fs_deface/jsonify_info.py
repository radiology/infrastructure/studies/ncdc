import argparse
import xnat
import logging
import json
import yaml

def find_file(scan):
    
    if not 'DICOM' in scan.resources and 'NIFTI' in scan.resources:
        
        file_type = 'NIFTI'
        transfer_syntax= None
        # if scan is not empty
        if len(scan.resources['NIFTI'].files) >0:
            # TODO: this only takes the first file, improve it
            filename = scan.resources['NIFTI'].files[0].id 
            file_path = scan.external_uri(scheme='xnat') + '/resources/NIFTI/files/' + filename
        else:
            return '',''
    elif 'DICOM' in scan.resources:
        file_type= "DICOM"
        file_path =  scan.external_uri(scheme='xnat') + '/resources/DICOM'

        #find out transfer syntax
        if scan.dicom_dump()[3]['tag1']== '(0002,0010)':
            transfer_syntax = scan.dicom_dump()[3]['value']
        else: #look for tag in dicom_dump
            for i in scan.dicom_dump(): 
                if i['tag1']== '(0002,0010)':
                    transfer_syntax = i['value']
    else:
        file_type = None
        file_path= None
        transfer_syntax= None

    return file_type, file_path, transfer_syntax


def info_experiments(xnat_project, subjects, t1_name, slice_threshold=10):
    ''' Used to get experiments' information from all subjects'''
    samples = {}
    for subject in subjects:
        
        xnat_subject = xnat_project.subjects[subject]
        logging.info(f"checking Subject: {xnat_subject.label}")

        for exp in xnat_subject.experiments.listing:

            if 'DEFACED' in exp.label:
                continue
            else:
                logging.debug(f"checking {exp.label}")
                sample_id = exp.label
                sample = {
                    'subject_label': xnat_subject.label,
                    'experiment_label':exp.label,
                    't1_scan': {},
                    'scans' :{}
                }
            
                for scan_id in exp.scans:   
                    
                    scan = exp.scans[scan_id]
                    logging.debug(f"Scan {scan.type}")
                    file_type, file_path, transfer_syntax = find_file(scan)

                    
                    if t1_name in scan.type:
                        sample['t1_scan']['id'] = scan.id
                        sample['t1_scan']['file_path'] = file_path
                        

                        if file_type == 'NIFTI':
                            sample['t1_scan']['file_type'] = 'NIFTI'
                            
                        elif file_type == 'DICOM':
                            # only include scans with slices >10 ...this excludes survey scans
                            if len(scan.resources['DICOM'].files) > slice_threshold:
                                sample['t1_scan']['file_type'] = 'DICOM' 
                                sample['t1_scan']['transfer_syntax'] = transfer_syntax
                            else:
                                sample['t1_scan']['file_type'] = None
                                sample['t1_scan']['transfer_syntax'] = None
                                
                        else:
                            continue
                       
                        

                    else:
                        sample['scans'][scan.type] = {}
                        sample['scans'][scan.type]['id'] = scan.id
                        sample['scans'][scan.type]['file_path'] = file_path
                        

                        if file_type == 'NIFTI':
                                sample['scans'][scan.type]['file_type'] = 'NIFTI'

                        elif file_type == 'DICOM':
                            # only include scans with slices >10 ...this excludes survey scans
                            if len(scan.resources['DICOM'].files) > slice_threshold: 
                                sample['scans'][scan.type]['file_type'] = 'DICOM' 
                                sample['scans'][scan.type]['transfer_syntax'] = transfer_syntax
                            else:
                                sample['scans'][scan.type]['file_type'] = None 
                                sample['scans'][scan.type]['transfer_syntax'] = None
                        else:
                            continue
                        
            
                    samples[sample['experiment_label']] = sample
                       
    return samples

def jsonify(project, output, t1_name, slice_threshold=10):
    xnat_project = project
    sample_list = {
        "version": "0.2", 
        "samples":{}
    }
    subjects = [xnat_subject for xnat_subject in xnat_project.subjects]
    sample_list['samples'] = info_experiments (xnat_project, subjects, t1_name, slice_threshold)

    with open(output, 'wt') as file_object:
        file_object.write(yaml.safe_dump(sample_list, indent=2))
    


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', required=True, help='xnat host')
    parser.add_argument('--project', required=True, help='xnat project')
    parser.add_argument('--output', required=True, help='yaml file')
    parser.add_argument('--t1_name', required=False, help='generic T1 scan name (used in xnat)')
 
    args = parser.parse_args()
    
    if args.t1_name:
        t1_name = args.t1_name
    else:
        t1_name = 'T1'

    with xnat.connect(args.host) as xnat_host:
        if args.project in xnat_host.projects:
            xnat_project = xnat_host.projects[args.project]
        else:
            logging.error(f"Project: {args.project} not found." )
            
        jsonify(xnat_project,  args.output, t1_name, 10)
    
if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main()
