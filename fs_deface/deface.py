from urllib.parse import urlparse, urlunparse
import logging 
import os
import jsonify_info
import json, yaml
import fastr, imp 
import re
import json
import xnat
import time
import io 
import sys

def run_network (source_mapping, sink_mapping, module, tmpdir, isDICOM, transfer_syntax, T1_dcm =None, t1_syntax=None): 
    logging.info(f" Running network module {module}\nSource Mapping:{source_mapping}\nSink Mapping: {sink_mapping}") 
    network = module.create_network(isDICOM, transfer_syntax, T1_dcm, t1_syntax)
    network.execute(source_mapping, sink_mapping, tmpdir=tmpdir) 
    
def create_experiment_defaced(xnat_uri, experiment, scan_id):
    logging.info(f" Creating new defaced experiment for Experiment: {experiment.label} {experiment.subject.label}")                                                                                                           

    dest_xnat = xnat.connect(xnat_uri)
    
    #TO DO: check if experiment is already created 
    label = 'DEFACED_' + experiment.label
    
    # create empty experiment
    dest_class = dest_xnat.XNAT_CLASS_LOOKUP.get(experiment.__xsi_type__)
    if dest_class is None:
        logging.warning('[WARNING] {experiment.__xsi_type__} class not found on destination server, skipping')
      
    dest_experiment = dest_class(parent=experiment.subject, label=label)
    logging.info(f" Created experiment {label}")

    
    new_scan = create_scan_defaced(xnat_uri, experiment, scan_id)
    return new_scan

def create_scan_defaced(xnat_uri, experiment, scan_id):
    logging.info(f" Creating new scan for Experiment: {experiment.label} {experiment.subject.label}")
    
    dest_xnat = xnat.connect(xnat_uri)
    project_label = experiment.subject.project
    subject_label = experiment.subject.label
    scan_type = experiment.scans[scan_id].type

    new_experiment = dest_xnat.projects[project_label].subjects[subject_label].experiments['DEFACED_'+experiment.label]
    xnat_resource = dest_xnat.classes.MrsScanData(parent=new_experiment,
                                        id =experiment.scans[scan_id].id,
                                        type = experiment.scans[scan_id].type,
                                        quality = experiment.scans[scan_id].quality,
                                        note = experiment.scans[scan_id].note)
    if 'NIFTI' not in new_experiment.scans[scan_id].resources:
        xnat_resource = dest_xnat.classes.ResourceCatalog(parent=new_experiment.scans[scan_id], label = 'NIFTI')

    return new_experiment.scans[scan_id]

def create_source_sink_data(T1_location, defaced_scan, T1_mask = None, nonT1_location = None, sink_loc=None):
    logging.info(f" Creating source and sink data ...")
    
    
    if defaced_scan is not None:
        defaced_location = defaced_scan.external_uri(scheme='xnat')
        #TO DO: put the path for templates in .env/config file
        source_mapping= {'source_image': T1_location + '?insecure=false',
                        'face_template': os.environ['FACE_TEMPLATE'],
                        'brain_template': os.environ['TALAIRACH']}
        
        sink_mapping= {'defacedMRI': defaced_location + '/resources/NIFTI/files/'+ ('-'.join(defaced_scan.id.split(' ')))  + '{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData&insecure=false',
                      'defacedMask':defaced_location + '/resources/NIFTI/files/'+ ('-'.join(defaced_scan.id.split(' '))) + '_mask{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData&insecure=false'}
    else:
        source_mapping= {'source_image':  nonT1_location + '?insecure=false',
                        'fixed_image': T1_location + '?insecure=false',
                        #'T1_mask': T1_mask + '?insecure=false',
                        'param_file': os.environ['PARAMETER_FILE'],
                        'face_template': os.environ['FACE_TEMPLATE'],
                        'brain_template': os.environ['TALAIRACH']}

        sink_mapping= {'defacedMRI': sink_loc + '{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData&insecure=false',
                      'defacedMask': sink_loc + '_mask{ext}?resource_type=xnat%3AresourceCatalog&assessors_type=xnat%3AqcAssessmentData&insecure=false'}

  
    return source_mapping, sink_mapping

def get_blueprint(project, output, t1_name=None):
    logging.info(f" Generating summary info (yaml) ...")
    jsonify_info.jsonify(project, output, t1_name)
    
    
def deface_T1(module_t1, xnat_uri, source_project, summary):
    logging.info(f" Defacing T1 scan in experiment {summary['experiment_label']}")  
    t1_summary = summary['t1_scan']
    file_type = t1_summary['file_type']
    
    if t1_summary['file_path'] is not None and t1_summary['file_type'] is not None:

        if file_type == 'NIFTI':
            isDICOM = False
            
        elif file_type =='DICOM':
            isDICOM = True
            
        else:
            logging.error(f"T1 source file is not in DICOM/NIFTI format for Experiment {summary['experiment_label']}")
            return

        # create new experiment for defaced T1 
        T1_experiment = source_project.subjects[summary['subject_label']].experiments[summary['experiment_label']]
        defacd_scan = create_experiment_defaced(xnat_uri, T1_experiment, t1_summary['id'])

        #create source and sink mapping 
        tmpdir=f"/tmp/{t1_summary['id']}_{time.time()}"
        source_mapping, sink_mapping = create_source_sink_data(t1_summary['file_path'], defacd_scan)

        #run fastr pipeline
        logging.debug(f"Defacing scan: {summary['t1_scan']} Experiment: {summary['experiment_label']}  Subject: {summary['subject_label']} ")  
        run_network(source_mapping, sink_mapping, module_t1, tmpdir, isDICOM, t1_summary['transfer_syntax'])
    else:
        logging.error(f"T1 info for Experiment {summary['experiment_label']} of Subject {summary['subject_label']} is not complete!")
        

def deface_nonT1(module_nont1, xnat_uri, source_project, summary):
    logging.info(f" Defacing all non T1 scans in experiment {summary['experiment_label']}") 
    nont1_summary = summary['scans']
    t1_summary = summary['t1_scan']

    if t1_summary['file_path'] is not None and t1_summary['file_type'] is not None:
        file_type = t1_summary['file_type']
        T1_location = t1_summary['file_path']
        experiment_label = summary['experiment_label']
        subject_label =summary['subject_label']

        if file_type == 'NIFTI':
            T1_dcm = False
            
        elif file_type =='DICOM':
            T1_dcm = True
            
        else:
            logging.error(f"T1 source file is not in DICOM/NIFTI format for Experiment {experiment_label} of Subject {subject_label}")
            return


        # Deface each scan
        for idx in nont1_summary:
            scan = nont1_summary[idx]
            scan_id = scan['id']

            # create new scan
            experiment = source_project.subjects[subject_label].experiments[experiment_label]
            defaced_scan = create_scan_defaced(xnat_uri, experiment, scan_id)

            if scan['file_path'] is not None and scan['file_type'] is not None:
                nonT1_location = scan['file_path']

                if scan['file_type'] == 'NIFTI':
                    isDICOM = False
                    nonT1_location = scan['NIFTI']
                elif scan['file_type'] == 'DICOM':
                    isDICOM = True   
                else:
                    logging.error(f" Scan type is not mentioned in DICOM/NIFTI format for Experiment {experiment_label} of Subject {subject_label}")
                    return

                #create source and sink mapping 
                tmpdir=f'/tmp/{scan_id}_{time.time()}'
                sink_loc = defaced_scan.external_uri(scheme='xnat') + '/resources/NIFTI/files/'+  ('-'.join(idx.split(' ')))
                source_mapping, sink_mapping = create_source_sink_data(T1_location, None, None, nonT1_location, sink_loc)

                #run fastr pipeline
                logging.info(f'Defacing scan: {idx} Experiment: {experiment_label}  Subject: {subject_label} ')  
                run_network(source_mapping, sink_mapping, module_nont1, tmpdir, isDICOM, scan['transfer_syntax'], T1_dcm, t1_summary['transfer_syntax'])
                #break
    

def main():
    import argparse
    import sys
              
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', required=True, help='xnat host')
    parser.add_argument('--project', required=True, help='xnat project')
    parser.add_argument('--t1_name', required=False, help='generic T1 scan name (used in xnat)')
    parser.add_argument('--log_INFO', required=False, help='sets logginglevel to INFO. Default is ERROR')


    args = parser.parse_args()

    # set logging settings
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    rootLogger = logging.getLogger()
    fileHandler = logging.FileHandler("{0}/{1}.log".format('/tmp', 'deface'))

    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

    # fastr logging setting
    fastr.config.log_to_file=True
        
    if args.t1_name:
        t1_name = args.t1_name
    else:
        t1_name = 'T1'
    

    if args.log_INFO:
        logging.getLogger().setLevel(logging.INFO)
    else:
        logging.getLogger().setLevel(logging.ERROR)
    
    #load modules
    module_T1 = imp.load_source("deface_t1", os.environ['MODULE_T1'])
    module_nonT1 = imp.load_source("deface_nont1", os.environ['MODULE_nonT1'])

    with xnat.connect(args.host) as xnat_host:
        if args.project in xnat_host.projects:
            logging.info(f"***** NCDC defacing pipeline ********")
            logging.info(f"-----Defacing scans on project {args.project}-----\n\n\n")

            xnat_project = xnat_host.projects[args.project]

            # generate info
            info_file = 'summary.yaml'
            get_blueprint(xnat_project, info_file, t1_name)

            #Iterate through project info(yaml) and deface each experiment
            with open(info_file, 'r') as fin:
                try:
                    blueprint = yaml.safe_load(fin)
                except ValueError as exception:
                    raise ValueError('{} ({})'.format(exception.args[0], info_file))

            for exp in blueprint['samples']:

                summary = blueprint['samples'][exp]
                try:
                    deface_T1(module_T1, args.host, xnat_project, summary)
                except:
                    print("Could not deface T1:", sys.exc_info()[0])
                    continue
                    
                try:
                    deface_nonT1(module_nonT1, args.host, xnat_project, summary)
                except:
                    print("Could not deface non T1 scans:", sys.exc_info()[0])
                    continue
                break
        else:
            logging.error(f"Project: {args.project} not found.")

if __name__ == '__main__':
    main()
   
    
